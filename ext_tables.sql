#
# Table structure for table 'tx_hivecptnavanchor_domain_model_navigation'
#
CREATE TABLE tx_hivecptnavanchor_domain_model_navigation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted smallint(5) unsigned DEFAULT '0' NOT NULL,
	hidden smallint(5) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state smallint(6) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

#
# Table structure for table 'pages'
#
CREATE TABLE pages (
  tx_hivecptanchornav_bs4_class_section varchar(255) DEFAULT '' NOT NULL,

  tx_hivecptanchornav_bs4_align_row varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_no_gutters_row varchar(255) DEFAULT '' NOT NULL,

  tx_hivecptanchornav_bs4_align_col0 varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_align_col1 varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_align_col2 varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_align_col3 varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_align_col4 varchar(255) DEFAULT '' NOT NULL,

  tx_hivecptanchornav_bs4_push_pull_col0 varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_push_pull_col1 varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_push_pull_col2 varchar(255) DEFAULT '' NOT NULL,
  tx_hivecptanchornav_bs4_push_pull_col3 varchar(255) DEFAULT '' NOT NULL,

);