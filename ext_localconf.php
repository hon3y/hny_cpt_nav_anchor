<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptNavAnchor',
            'Hivecptnavanchornavigationrendernavanchor',
            [
                'Navigation' => 'renderNavAnchor'
            ],
            // non-cacheable actions
            [
                'Navigation' => 'renderNavAnchor'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptnavanchornavigationrendernavanchor {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_nav_anchor') . 'Resources/Public/Icons/user_plugin_hivecptnavanchornavigationrendernavanchor.svg
                        title = LLL:EXT:hive_cpt_nav_anchor/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_nav_anchor_domain_model_hivecptnavanchornavigationrendernavanchor
                        description = LLL:EXT:hive_cpt_nav_anchor/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_nav_anchor_domain_model_hivecptnavanchornavigationrendernavanchor.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptnavanchor_hivecptnavanchornavigationrendernavanchor
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptnavanchornavigationrendernavanchor >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptnavanchornavigationrendernavanchor {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Anchor Navigation
                            description = Anchor navigation for selected page
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptnavanchor_hivecptnavanchornavigationrendernavanchor
                            }
                        }
                        show := addToList(hivecptnavanchornavigationrendernavanchor)
                    }

                }
            }'
        );

    }, $_EXTKEY
);