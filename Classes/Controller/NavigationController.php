<?php
namespace HIVE\HiveCptNavAnchor\Controller;

/***
 *
 * This file is part of the "hive_cpt_nav_anchor" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * NavigationController
 */
class NavigationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * action renderNavAnchor
     *
     * @return void
     */
    public function renderNavAnchorAction()
    {
        $aSettings = $this->settings;
        $iOnePageUid = intval($aSettings['navanchor']);
        $iPageUid = $this->getFirstSubPid($iOnePageUid);

        $this->view->assign('iOnePageUid',$iOnePageUid);
        $this->view->assign('iPageUid',$iPageUid);
    }

    private function getFirstSubPid($parent = 0){
        $depth = 1;
        $queryGenerator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance( 'TYPO3\\CMS\\Core\\Database\\QueryGenerator' );
        $childPids = $queryGenerator->getTreeList($parent, $depth, 0, 1); //Will be a string like 1,2,3
        $childPids = explode(',',$childPids );
        return intval($childPids[1]);
    }
}
