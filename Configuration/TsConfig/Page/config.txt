###################################################
#### BACKENDLAYOUTS EXT:hive_cpt_nav_anchor ####
###################################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cpt_nav_anchor/Resources/Private/BackendLayouts" extensions="txt">

################################################
#### PAGELAYOUTS EXT:hive_cpt_nav_anchor ####
################################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cpt_nav_anchor/Configuration/TsConfig/Page/Production" extensions="txt">

