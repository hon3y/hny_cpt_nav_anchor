<?php

// Also add the new doktype to the page language overlays type selector (so that translations can inherit the same type)
call_user_func(
    function ($extKey, $table) {

        /*
         * Section A
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $customPageIcon = $extRelPath . 'Resources/Public/Icons/apps-pagetree-section.svg';
        $iDoktype = 116;

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_section.label',
                $iDoktype,
                $customPageIcon
            ],
            '1',
            'after'
        );

        /*
         * Section B
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $customPageIcon = $extRelPath . 'Resources/Public/Icons/apps-pagetree-section-b.svg';
        $iDoktype = 117;

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_section_b.label',
                $iDoktype,
                $customPageIcon
            ],
            '1',
            'after'
        );

        /*
         * Section C
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $customPageIcon = $extRelPath . 'Resources/Public/Icons/apps-pagetree-section-c.svg';
        $iDoktype = 118;

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_section_c.label',
                $iDoktype,
                $customPageIcon
            ],
            '1',
            'after'
        );

        /*
         * Row
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $customPageIcon = $extRelPath . 'Resources/Public/Icons/apps-pagetree-row.svg';
        $iDoktype = 119;

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_row.label',
                $iDoktype,
                $customPageIcon
            ],
            '1',
            'after'
        );

    },
    'hive_cpt_nav_anchor',
    'pages_language_overlay'
);