<?php

defined('TYPO3_MODE') or die();



$tca_pages = [
    'ctrl' => [
        'type' => 'doktype',
    ],
    'palettes' => [
        'bs4' => [
            'showitem' => '--linebreak--,tx_hivecptanchornav_bs4_class_section,--linebreak--,tx_hivecptanchornav_bs4_align_row,tx_hivecptanchornav_bs4_no_gutters_row,--linebreak--,
            tx_hivecptanchornav_bs4_align_col0,tx_hivecptanchornav_bs4_align_col1,tx_hivecptanchornav_bs4_align_col2,tx_hivecptanchornav_bs4_align_col3,--linebreak--,
            tx_hivecptanchornav_bs4_align_col4--linebreak--,
            tx_hivecptanchornav_bs4_push_pull_col0,tx_hivecptanchornav_bs4_push_pull_col1,tx_hivecptanchornav_bs4_push_pull_col2,tx_hivecptanchornav_bs4_push_pull_col3',
        ],
    ],


    'columns' => [
        'tx_hivecptanchornav_bs4_class_section' => [
            'exclude' => 1,
            'label' => 'custom section-classes',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_align_row' => [
            'exclude' => 1,
            'label' => 'row align-items-...',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_no_gutters_row' => [
            'exclude' => 1,
            'label' => 'row no-gutters...',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],

        'tx_hivecptanchornav_bs4_align_col0' => [
            'exclude' => 1,
            'label' => 'col align-self-... (1)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_align_col1' => [
            'exclude' => 1,
            'label' => 'col align-self-... (2)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_align_col2' => [
            'exclude' => 1,
            'label' => 'col align-self-... (3)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_align_col3' => [
            'exclude' => 1,
            'label' => 'col align-self-... (4)',
            'config' => [
                'items' => [
                    ['---', 0]
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_align_col4' => [
            'exclude' => 1,
            'label' => 'col align-self-... (5)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],

        'tx_hivecptanchornav_bs4_push_pull_col0' => [
            'exclude' => 1,
            'label' => 'col push-... pull-... (1)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_push_pull_col1' => [
            'exclude' => 1,
            'label' => 'col push-... pull-... (2)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_push_pull_col2' => [
            'exclude' => 1,
            'label' => 'col push-... pull-... (3)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'tx_hivecptanchornav_bs4_push_pull_col3' => [
            'exclude' => 1,
            'label' => 'col push-... pull-... (4)',
            'config' => [
                'items' => [
                    ['---', '']
                ],
                'type' => 'select',
                'renderType' => 'selectSingle',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],

    ],
];

$GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] = 'backend_layout';
$GLOBALS['TCA']['pages']['columns']['backend_layout']['onchange'] = 'reload';

//$GLOBALS['TCA']['pages'] = array_replace_recursive($GLOBALS['TCA']['pages'], $tca_pages);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'pages',
    $tca_pages['columns']
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'pages',
    'layout',
    $tca_pages['palettes']['bs4']['showitem'],
    'after:new_until'
);

//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
//    'pages',
//    $tca_pages['palettes']['bs4']
//);

call_user_func(
    function ($extKey, $table) {

        /*
         * Section A
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $iDoktype = 116;


        // Provide icon for page tree, list view, ... :
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
            ->registerIcon(
                'apps-pagetree-section',
                TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                [
                    'source' => 'EXT:' . $extKey . '/Resources/Public/Icons/apps-pagetree-section.svg',
                ]
            );


        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_section.label',
                $iDoktype,
                'apps-pagetree-section'
            ],
            '1',
            'after'
        );

        // Add icon for new page type:
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TCA']['pages'],
            [
                'ctrl' => [
                    'typeicon_classes' => [
                        $iDoktype => 'apps-pagetree-section',
                    ],
                ],
            ]
        );

        /*
         * Section B
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $iDoktype = 117;

        // Provide icon for page tree, list view, ... :
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
            ->registerIcon(
                'apps-pagetree-section-b',
                TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                [
                    'source' => 'EXT:' . $extKey . '/Resources/Public/Icons/apps-pagetree-section-b.svg',
                ]
            );

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_section_b.label',
                $iDoktype,
                'apps-pagetree-section-b'
            ],
            '1',
            'after'
        );

        // Add icon for new page type:
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TCA']['pages'],
            [
                'ctrl' => [
                    'typeicon_classes' => [
                        $iDoktype => 'apps-pagetree-section-b',
                    ],
                ],
            ]
        );

        /*
         * Section C
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $iDoktype = 118;

        // Provide icon for page tree, list view, ... :
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
            ->registerIcon(
                'apps-pagetree-section-c',
                TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                [
                    'source' => 'EXT:' . $extKey . '/Resources/Public/Icons/apps-pagetree-section-c.svg',
                ]
            );

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_section_c.label',
                $iDoktype,
                'apps-pagetree-section-c'
            ],
            '1',
            'after'
        );

        // Add icon for new page type:
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TCA']['pages'],
            [
                'ctrl' => [
                    'typeicon_classes' => [
                        $iDoktype => 'apps-pagetree-section-c',
                    ],
                ],
            ]
        );

        /*
         * Row
         */
        $extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey);
        $iDoktype = 119;

        // Provide icon for page tree, list view, ... :
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
            ->registerIcon(
                'apps-pagetree-row',
                TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                [
                    'source' => 'EXT:' . $extKey . '/Resources/Public/Icons/apps-pagetree-row.svg',
                ]
            );

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/translation.xlf:apps_pagetree_row.label',
                $iDoktype,
                'apps-pagetree-row'
            ],
            '1',
            'after'
        );

        // Add icon for new page type:
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TCA']['pages'],
            [
                'ctrl' => [
                    'typeicon_classes' => [
                        $iDoktype => 'apps-pagetree-row',
                    ],
                ],
            ]
        );

    },
    'hive_cpt_nav_anchor',
    'pages'
);