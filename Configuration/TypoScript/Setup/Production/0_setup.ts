
plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor {
    view {
        templateRootPaths.0 = EXT:hive_cpt_nav_anchor/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_nav_anchor/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_nav_anchor/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hivecptnavanchor._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-cpt-nav-anchor table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-cpt-nav-anchor table th {
        font-weight:bold;
    }

    .tx-hive-cpt-nav-anchor table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

#############################
## NAVIGATION ANCHOR SETUP ##
#############################

lib.navigation.anchor = COA
lib.navigation.anchor {

    5 = LOAD_REGISTER
    5  {

        colPos.cObject = TEXT
        colPos.cObject {
            field = colPos
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = 0
            }
        }

        pageUid.cObject = TEXT
        pageUid.cObject {
            field = pageUid
            ifEmpty.data = TSFE:id
        }

        onePageUid.cObject = TEXT
        onePageUid.cObject {
            field = onePageUid
            ifEmpty.data = TSFE:id
        }

    }
    20 = CONTENT
    20 {
        table = pages
        select {
            where.insertData = 1
            pidInList.data = register:pageUid
            #pidInList = 1
            where = (NOT hidden) AND (NOT deleted) AND (NOT nav_hide)
            orderBy = sorting
        }
        wrap = <ul>|</ul>

        renderObj = COA
        renderObj {
            10 = TEXT
            10.field = nav_title // title

            10.typolink {
                wrap = <li>|</li>
                parameter.data = register:onePageUid
                #additionalParams = #page-{field:uid}
                section.dataWrap = page-{field:uid}
            }
            #10.stdWrap.dataWrap = <span>{register:onePageUid} |</span>
            #10.wrap = |
            #10.stdWrap.dataWrap = <a href="index.php?id={field:pid}" data-target-anchor="#page-{field:uid}" data-target-page="{field:pid}">|</a>

        }
    }

    30 = RESTORE_REGISTER
}

[globalVar = LIT:1 = {$plugin.tx_hive_cpt_nav_anchor.settings.lib.navigation.bShowStart}]
    lib.navigation.anchor.7 = COA
    lib.navigation.anchor.7 {
        10 = TEXT
        10.typolink.parameter.data = register:onePageUid
        10.wrap = <h2 class="specialValue">|</h2>
    }
[global]

#######################
## PAGE ANCHOR SETUP ##
#######################

lib.site.anchor = COA
lib.site.anchor {

    #
    5 = LOAD_REGISTER
    5  {

        ###
        ### 100%
        ###
        sectionClasses100.cObject = TEXT
        sectionClasses100.cObject {
            field = sectionClasses100
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses100.cObject = TEXT
        containerDivClasses100.cObject {
            field = containerDivClasses100
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses100.cObject = TEXT
        rowDivClasses100.cObject {
            field = rowDivClasses100
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses100.cObject = TEXT
        colDivClasses100.cObject {
            field = colDivClasses100
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col
            }
        }
        offsetClasses100.cObject = TEXT
        offsetClasses100.cObject {
            field = offsetClasses100
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 50% 50%
        ###
        sectionClasses5050.cObject = TEXT
        sectionClasses5050.cObject {
            field = sectionClasses5050
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses5050.cObject = TEXT
        containerDivClasses5050.cObject {
            field = containerDivClasses5050
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses5050.cObject = TEXT
        rowDivClasses5050.cObject {
            field = rowDivClasses5050
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses5050a.cObject = TEXT
        colDivClasses5050a.cObject {
            field = colDivClasses5050a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-6
            }
        }
        colDivClasses5050b.cObject = TEXT
        colDivClasses5050b.cObject {
            field = colDivClasses5050b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-6
            }
        }
        offsetClasses5050.cObject = TEXT
        offsetClasses5050.cObject {
            field = offsetClasses5050
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 33% 66%
        ###
        sectionClasses3366.cObject = TEXT
        sectionClasses3366.cObject {
            field = sectionClasses3366
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses3366.cObject = TEXT
        containerDivClasses3366.cObject {
            field = containerDivClasses3366
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses3366.cObject = TEXT
        rowDivClasses3366.cObject {
            field = rowDivClasses3366
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses3366a.cObject = TEXT
        colDivClasses3366a.cObject {
            field = colDivClasses3366a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-4
            }
        }
        colDivClasses3366b.cObject = TEXT
        colDivClasses3366b.cObject {
            field = colDivClasses3366b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-8
            }
        }
        offsetClasses3366.cObject = TEXT
        offsetClasses3366.cObject {
            field = offsetClasses3366
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 66% 33%
        ###
        sectionClasses6633.cObject = TEXT
        sectionClasses6633.cObject {
            field = sectionClasses6633
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses6633.cObject = TEXT
        containerDivClasses6633.cObject {
            field = containerDivClasses6633
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses6633.cObject = TEXT
        rowDivClasses6633.cObject {
            field = rowDivClasses6633
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses6633a.cObject = TEXT
        colDivClasses6633a.cObject {
            field = colDivClasses6633a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-8
            }
        }
        colDivClasses6633b.cObject = TEXT
        colDivClasses6633b.cObject {
            field = colDivClasses6633b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-4
            }
        }
        offsetClasses6633.cObject = TEXT
        offsetClasses6633.cObject {
            field = offsetClasses6633
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 33% 33% 33%
        ###
        sectionClasses333333.cObject = TEXT
        sectionClasses333333.cObject {
            field = sectionClasses333333
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses333333.cObject = TEXT
        containerDivClasses333333.cObject {
            field = containerDivClasses333333
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses333333.cObject = TEXT
        rowDivClasses333333.cObject {
            field = rowDivClasses333333
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses333333a.cObject = TEXT
        colDivClasses333333a.cObject {
            field = colDivClasses333333a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-4
            }
        }
        colDivClasses333333b.cObject = TEXT
        colDivClasses333333b.cObject {
            field = colDivClasses333333b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-4
            }
        }
        colDivClasses333333c.cObject = TEXT
        colDivClasses333333c.cObject {
            field = colDivClasses333333c
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-4
            }
        }
        offsetClasses333333.cObject = TEXT
        offsetClasses333333.cObject {
            field = offsetClasses333333
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 25% 25% 25% 25%
        ###
        sectionClasses25252525.cObject = TEXT
        sectionClasses25252525.cObject {
            field = sectionClasses25252525
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses25252525.cObject = TEXT
        containerDivClasses25252525.cObject {
            field = containerDivClasses25252525
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses25252525.cObject = TEXT
        rowDivClasses25252525.cObject {
            field = rowDivClasses25252525
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses25252525a.cObject = TEXT
        colDivClasses25252525a.cObject {
            field = colDivClasses25252525a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-3
            }
        }
        colDivClasses25252525b.cObject = TEXT
        colDivClasses25252525b.cObject {
            field = colDivClasses25252525b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-3
            }
        }
        colDivClasses25252525c.cObject = TEXT
        colDivClasses25252525c.cObject {
            field = colDivClasses25252525c
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-3
            }
        }
        colDivClasses25252525d.cObject = TEXT
        colDivClasses25252525d.cObject {
            field = colDivClasses25252525d
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-3
            }
        }
        offsetClasses25252525.cObject = TEXT
        offsetClasses25252525.cObject {
            field = offsetClasses25252525
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 25% 75%
        ###
        sectionClasses2575.cObject = TEXT
        sectionClasses2575.cObject {
            field = sectionClasses2575
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses2575.cObject = TEXT
        containerDivClasses2575.cObject {
            field = containerDivClasses2575
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses2575.cObject = TEXT
        rowDivClasses2575.cObject {
            field = rowDivClasses2575
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses2575a.cObject = TEXT
        colDivClasses2575a.cObject {
            field = colDivClasses2575a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-3
            }
        }
        colDivClasses2575b.cObject = TEXT
        colDivClasses2575b.cObject {
            field = colDivClasses2575b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-9
            }
        }
        offsetClasses2575.cObject = TEXT
        offsetClasses2575.cObject {
            field = offsetClasses2575
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 75% 25%
        ###
        sectionClasses7525.cObject = TEXT
        sectionClasses7525.cObject {
            field = sectionClasses2575
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses7525.cObject = TEXT
        containerDivClasses7525.cObject {
            field = containerDivClasses7525
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses7525.cObject = TEXT
        rowDivClasses7525.cObject {
            field = rowDivClasses7525
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses7525a.cObject = TEXT
        colDivClasses7525a.cObject {
            field = colDivClasses7525a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-9
            }
        }
        colDivClasses7525b.cObject = TEXT
        colDivClasses7525b.cObject {
            field = colDivClasses7525b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-3
            }
        }
        offsetClasses7525.cObject = TEXT
        offsetClasses7525.cObject {
            field = offsetClasses7525
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 40% 60%
        ###
        sectionClasses4060.cObject = TEXT
        sectionClasses4060.cObject {
            field = sectionClasses4060
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses4060.cObject = TEXT
        containerDivClasses4060.cObject {
            field = containerDivClasses4060
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses4060.cObject = TEXT
        rowDivClasses4060.cObject {
            field = rowDivClasses4060
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses4060a.cObject = TEXT
        colDivClasses4060a.cObject {
            field = colDivClasses4060a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-5
            }
        }
        colDivClasses4060b.cObject = TEXT
        colDivClasses4060b.cObject {
            field = colDivClasses4060b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-7
            }
        }
        offsetClasses4060.cObject = TEXT
        offsetClasses4060.cObject {
            field = offsetClasses4060
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }
        ###
        ### 60% 40%
        ###
        sectionClasses6040.cObject = TEXT
        sectionClasses6040.cObject {
            field = sectionClasses6040
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses6040.cObject = TEXT
        containerDivClasses6040.cObject {
            field = containerDivClasses6040
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses6040.cObject = TEXT
        rowDivClasses6040.cObject {
            field = rowDivClasses6040
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses6040a.cObject = TEXT
        colDivClasses6040a.cObject {
            field = colDivClasses6040a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-7
            }
        }
        colDivClasses6040b.cObject = TEXT
        colDivClasses6040b.cObject {
            field = colDivClasses6040b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-5
            }
        }
        offsetClasses6040.cObject = TEXT
        offsetClasses6040.cObject {
            field = offsetClasses6040
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }

        ###
        ### 20% 20% 20% 20% 20%
        ###
        sectionClasses2020202020.cObject = TEXT
        sectionClasses2020202020.cObject {
            field = sectionClasses2020202020
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = section
            }
        }
        containerDivClasses2020202020.cObject = TEXT
        containerDivClasses2020202020.cObject {
            field = containerDivClasses2020202020
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = container
            }
        }
        rowDivClasses2020202020.cObject = TEXT
        rowDivClasses2020202020.cObject {
            field = rowDivClasses2020202020
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = row
            }
        }
        colDivClasses2020202020a.cObject = TEXT
        colDivClasses2020202020a.cObject {
            field = colDivClasses2020202020a
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-20
            }
        }
        colDivClasses2020202020b.cObject = TEXT
        colDivClasses2020202020b.cObject {
            field = colDivClasses2020202020b
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-20
            }
        }
        colDivClasses2020202020c.cObject = TEXT
        colDivClasses2020202020c.cObject {
            field = colDivClasses2020202020c
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-20
            }
        }
        colDivClasses2020202020d.cObject = TEXT
        colDivClasses2020202020d.cObject {
            field = colDivClasses2020202020d
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-20
            }
        }
        colDivClasses2020202020e.cObject = TEXT
        colDivClasses2020202020e.cObject {
            field = colDivClasses2020202020e
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = col-md-20
            }
        }
        offsetClasses2020202020.cObject = TEXT
        offsetClasses2020202020.cObject {
            field = offsetClasses2020202020
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = offset-md-1 col-md-10
            }
        }


        hClasses.cObject = TEXT
        hClasses.cObject {
            field = hClasses
            ifEmpty.cObject = TEXT
            ifEmpty.cObject {
                value.current = 1
                ifEmpty = text-align_center
            }
        }

        #        colPos.cObject = TEXT
        #        colPos.cObject {
        #            field = colPos
        #            ifEmpty.cObject = TEXT
        #            ifEmpty.cObject {
        #                value.current = 1
        #                ifEmpty = 0
        #            }
        #        }

        pageUid.cObject = TEXT
        pageUid.cObject {
            field = pageUid
            ifEmpty.data = TSFE:id
        }
    }
    #

    10 = CONTENT
    10 {
        table = pages
        select {
            where.insertData = 1
            pidInList.data = register:pageUid
            orderBy = sorting
        }

        renderObj = COA
        renderObj {

            10 = LOAD_REGISTER
            10 {

                meinRegister.cObject = TEXT
                meinRegister.cObject.field = uid
            }

            ###
            ### Section Wrap
            ###
            15 = COA
            15.stdWrap.dataWrap = <a id="page-{field:uid}" name="page-{field:uid}"></a><section id="section__uid--{field:uid}" data-section="{field:uid}" class="{register:sectionClasses100} section__bl--{field:backend_layout} section__doktype--{field:doktype} {field:tx_hivecptanchornav_bs4_class_section}" data-sys_language_uid="{TSFE:sys_language_uid}">|</section>

            15 {

                ###
                ### IMAGE as png
                ###
                800 = FILES
                800 {
                    references {
                        table = pages
                        uid.data = field = uid
                        fieldName = media
                    }

                    renderObj = TEXT
                    renderObj {
                        stdWrap.wrap = <div class="section__background"><div class="section__background--inner b-lazy opacity_0" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-echo="|"></div></div>
                        stdWrap.required = 1
                        data = file:current:publicUrl

                        if {
                            value = bg.png
                            equals.data = file:current:name
                        }
                    }
                }

                ###
                ### IMAGE as jpg
                ###
                900 = FILES
                900 {
                    references {
                        table = pages
                        uid.data = field = uid
                        fieldName = media
                    }

                    renderObj = TEXT
                    renderObj {
                        stdWrap.wrap = <div class="section__background"><div class="section__background--inner b-lazy opacity_0" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-echo="|"></div></div>
                        stdWrap.required = 1
                        data = file:current:publicUrl

                        if {
                            value = bg.jpg
                            equals.data = file:current:name
                        }
                    }
                }

                ###
                ### Suppages as Rows
                ###
                1000 = CONTENT
                1000 {
                    table = pages
                    select {
                        where.insertData = 1
                        pidInList.data = register:meinRegister
                        orderBy = sorting
                    }

                    renderObj = COA
                    renderObj {

                        10 = LOAD_REGISTER
                        10 {

                            pidRegister.cObject = TEXT
                            pidRegister.cObject.field = uid
                        }

                        ###
                        ### 100%
                        ###
                        20 = COA
                        20.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}"><div class="{register:rowDivClasses100} layout-{field:layout}">|</div></div>
                        20 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_col0}"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        20.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor100
                        }

                        ###
                        ### 100% with Offset
                        ###
                        25 = COA
                        25.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses100} layout-{field:layout}"><div class="{register:rowDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}"><div class="{register:offsetClasses100} layout-{field:layout}"><div class="{register:rowDivClasses100} layout-{field:layout}">|</div></div></div></div>
                        25 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses100} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_col0}"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        25.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor100WithOffset
                        }

                        ###
                        ### 50% 50%
                        ###
                        30 = COA
                        30.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses5050} layout-{field:layout}"><div class="{register:rowDivClasses5050} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        30 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses5050a} {field:tx_hivecptanchornav_bs4_align_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses5050b} {field:tx_hivecptanchornav_bs4_align_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        30.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor5050
                        }

                        ###
                        ### 50% 50% with Offset
                        ###
                        35 = COA
                        35.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses5050} layout-{field:layout}"><div class="{register:rowDivClasses5050} layout-{field:layout}"><div class="{register:offsetClasses5050}"><div class="{register:rowDivClasses5050} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        35 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses5050a} {field:tx_hivecptanchornav_bs4_align_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses5050b} {field:tx_hivecptanchornav_bs4_align_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        35.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor5050WithOffset
                        }

                        ###
                        ### 33% 66%
                        ###
                        40 = COA
                        40.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses3366} layout-{field:layout}"><div class="{register:rowDivClasses3366} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        40 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses3366a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses3366b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        40.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor3366
                        }

                        ###
                        ### 33% 66% with Offset
                        ###
                        45 = COA
                        45.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses3366} layout-{field:layout}"><div class="{register:rowDivClasses3366} layout-{field:layout}"><div class="{register:offsetClasses3366}"><div class="{register:rowDivClasses3366} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        45 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses3366a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses3366b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        45.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor3366WithOffset
                        }

                        ###
                        ### 66% 33%
                        ###
                        50 = COA
                        50.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses6633} layout-{field:layout}"><div class="{register:rowDivClasses6633} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        50 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses6633a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses6633b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        50.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor6633
                        }

                        ###
                        ### 66% 33% with Offset
                        ###
                        55 = COA
                        55.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses6633} layout-{field:layout}"><div class="{register:rowDivClasses6633} layout-{field:layout}"><div class="{register:offsetClasses6633}"><div class="{register:rowDivClasses6633} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        55 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses6633a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses6633b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        55.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor6633WithOffset
                        }

                        ###
                        ### 33% 33% 33%
                        ###
                        60 = COA
                        60.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses333333} layout-{field:layout}"><div class="{register:rowDivClasses333333} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        60 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses333333a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses333333b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            30 = CONTENT
                            30.stdWrap.dataWrap = <div class="{register:colDivClasses333333c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3"><div class="layout-{field:layout}">|</div></div>
                            30 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=2
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        60.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor333333
                        }

                        ###
                        ### 33% 33% 33% with Offset
                        ###
                        65 = COA
                        65.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses333333} layout-{field:layout}"><div class="{register:rowDivClasses333333} layout-{field:layout}"><div class="{register:offsetClasses333333}"><div class="{register:rowDivClasses333333} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        65 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses333333a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses333333b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            30 = CONTENT
                            30.stdWrap.dataWrap = <div class="{register:colDivClasses333333c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3"><div class="layout-{field:layout}">|</div></div>
                            30 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=2
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        65.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor333333WithOffset
                        }

                        ###
                        ### 25% 25% 25% 25%
                        ###
                        70 = COA
                        70.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses25252525} layout-{field:layout}"><div class="{register:rowDivClasses25252525} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        70 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses25252525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses25252525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            30 = CONTENT
                            30.stdWrap.dataWrap = <div class="{register:colDivClasses25252525c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3"><div class="layout-{field:layout}">|</div></div>
                            30 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=2
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            40 = CONTENT
                            40.stdWrap.dataWrap = <div class="{register:colDivClasses25252525d} {field:tx_hivecptanchornav_bs4_align_col3} {field:tx_hivecptanchornav_bs4_push_pull_col3} col--4"><div class="layout-{field:layout}">|</div></div>
                            40 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=3
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        70.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor25252525
                        }

                        ###
                        ### 25% 25% 25% 25% with Offset
                        ###
                        75 = COA
                        75.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses25252525} layout-{field:layout}"><div class="{register:rowDivClasses25252525} layout-{field:layout}"><div class="{register:offsetClasses25252525}"><div class="{register:rowDivClasses25252525} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        75 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses25252525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses25252525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            30 = CONTENT
                            30.stdWrap.dataWrap = <div class="{register:colDivClasses25252525c} {field:tx_hivecptanchornav_bs4_align_col2} {field:tx_hivecptanchornav_bs4_push_pull_col2} col--3"><div class="layout-{field:layout}">|</div></div>
                            30 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=2
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            40 = CONTENT
                            40.stdWrap.dataWrap = <div class="{register:colDivClasses25252525d} {field:tx_hivecptanchornav_bs4_align_col3} {field:tx_hivecptanchornav_bs4_push_pull_col3} col--4"><div class="layout-{field:layout}">|</div></div>
                            40 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=3
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        75.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor25252525WithOffset
                        }


                        ###
                        ### 25% 75%
                        ###
                        80 = COA
                        80.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses2575} layout-{field:layout}"><div class="{register:rowDivClasses2575} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        80 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses2575a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses2575b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        80.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor2575
                        }

                        ###
                        ### 25% 75% with Offset
                        ###
                        85 = COA
                        85.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses2575} layout-{field:layout}"><div class="{register:rowDivClasses2575} layout-{field:layout}"><div class="{register:offsetClasses2575}"><div class="{register:rowDivClasses2575} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        85 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses2575a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses2575b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        85.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor2575WithOffset
                        }

                        ###
                        ### 75% 25%
                        ###
                        90 = COA
                        90.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses7525} layout-{field:layout}"><div class="{register:rowDivClasses7525} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        90 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses7525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses7525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        90.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor7525
                        }

                        ###
                        ### 75% 25% with Offset
                        ###
                        95 = COA
                        95.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses7525} layout-{field:layout}"><div class="{register:rowDivClasses7525} layout-{field:layout}"><div class="{register:offsetClasses7525}"><div class="{register:rowDivClasses7525} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        95 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses7525a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses7525b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        95.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor7525WithOffset
                        }

                        ###
                        ### 40% 60%
                        ###
                        100 = COA
                        100.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses4060} layout-{field:layout}"><div class="{register:rowDivClasses4060} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        100 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses4060a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses4060b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        100.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor4060
                        }

                        ###
                        ### 40% 60% with Offset
                        ###
                        105 = COA
                        105.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses4060} layout-{field:layout}"><div class="{register:rowDivClasses4060} layout-{field:layout}"><div class="{register:offsetClasses4060}"><div class="{register:rowDivClasses4060} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        105 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses4060a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses4060b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        105.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor4060WithOffset
                        }

                        ###
                        ### 60% 40%
                        ###
                        110 = COA
                        110.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses6040} layout-{field:layout}"><div class="{register:rowDivClasses6040} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        110 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses6040a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses6040b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        110.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor6040
                        }

                        ###
                        ### 40% 60% with Offset
                        ###
                        115 = COA
                        115.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses6040} layout-{field:layout}"><div class="{register:rowDivClasses6040} layout-{field:layout}"><div class="{register:offsetClasses6040}"><div class="{register:rowDivClasses6040} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        115 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses6040a} {field:tx_hivecptanchornav_bs4_align_col0} {field:tx_hivecptanchornav_bs4_push_pull_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses6040b} {field:tx_hivecptanchornav_bs4_align_col1} {field:tx_hivecptanchornav_bs4_push_pull_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        115.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor6040WithOffset
                        }



                        ###
                        ### 20% 20% 20% 20% 20%
                        ###
                        120 = COA
                        120.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses2020202020} layout-{field:layout}"><div class="{register:rowDivClasses2020202020} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div>
                        120 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020a} {field:tx_hivecptanchornav_bs4_align_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020b} {field:tx_hivecptanchornav_bs4_align_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            30 = CONTENT
                            30.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020c} {field:tx_hivecptanchornav_bs4_align_col2} col--3"><div class="layout-{field:layout}">|</div></div>
                            30 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=2
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            40 = CONTENT
                            40.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020d} {field:tx_hivecptanchornav_bs4_align_col3} col--4"><div class="layout-{field:layout}">|</div></div>
                            40 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=3
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            50 = CONTENT
                            50.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020e} {field:tx_hivecptanchornav_bs4_align_col4} col--5"><div class="layout-{field:layout}">|</div></div>
                            50 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=4
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        120.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor2020202020
                        }

                        ###
                        ### 20% 20% 20% 20% 20% with Offset
                        ###
                        125 = COA
                        125.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:containerDivClasses2020202020} layout-{field:layout}"><div class="{register:rowDivClasses2020202020} layout-{field:layout}"><div class="{register:offsetClasses2020202020}"><div class="{register:rowDivClasses2020202020} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div></div></div></div>
                        125 {
                            10 = CONTENT
                            10.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020a} {field:tx_hivecptanchornav_bs4_align_col0} col--1"><div class="layout-{field:layout}">|</div></div>
                            10 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=0
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            20 = CONTENT
                            20.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020b} {field:tx_hivecptanchornav_bs4_align_col1} col--2"><div class="layout-{field:layout}">|</div></div>
                            20 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=1
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            30 = CONTENT
                            30.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020c} {field:tx_hivecptanchornav_bs4_align_col2} col--3"><div class="layout-{field:layout}">|</div></div>
                            30 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=2
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            40 = CONTENT
                            40.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020d} {field:tx_hivecptanchornav_bs4_align_col3} col--4"><div class="layout-{field:layout}">|</div></div>
                            40 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=3
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                            50 = CONTENT
                            50.stdWrap.dataWrap = <div class="{register:colDivClasses2020202020e} {field:tx_hivecptanchornav_bs4_align_col4} col--5"><div class="layout-{field:layout}">|</div></div>
                            50 {
                                ###
                                # styles.content.get can only read from current sys_language_uid
                                # therefor we use "select"
                                ###
                                table = tt_content
                                select {
                                    where.insertData = 1
                                    pidInList.data = register:pidRegister
                                    where = colPos=4
                                    andWhere.dataWrap = (NOT hidden) AND (NOT deleted) AND (sys_language_uid=-1 OR sys_language_uid=0 OR sys_language_uid={TSFE:sys_language_uid})
                                    orderBy = sorting
                                }
                            }

                        }
                        125.if {
                            value.field = backend_layout
                            equals = pagets__SectionAnchor2020202020WithOffset
                        }
                        ###


                    }

                } # / 1000

            } # / 15

        }
    }

    30 = RESTORE_REGISTER

}

########
# you can use this lib to automatically use the first subpage (of type "Menu Separator"!) in
# <f:cObject typoscriptObjectPath="lib.navigation.anchor" data="{pageUid: '{f:cObject(typoscriptObjectPath: \'lib.firstSubPage\')}'}" /> and
# and
# <f:cObject typoscriptObjectPath="lib.site.anchor" data="{pageUid: '{f:cObject(typoscriptObjectPath: \'lib.firstSubPage\')}'}" />
#
# If you use "content_from_pid" it will search first supage (of type "Menu Separator"!) of the TSFE:content_from_pid (where content comes from!)
#
########
lib.firstSubPage = CONTENT
lib.firstSubPage {
    table = pages
    select {
        #where = doktype=254 AND hidden=0
        where.insertData = 1
        pidInList.data = TSFE:content_from_pid
        orderBy = sorting ASC
        max = 1
    }
    renderObj = TEXT
    renderObj {
        data = field:uid
        #value = Link to first child page
        #typolink {
        #  parameter.field = uid
        #}
    }
}
[globalVar = TSFE:content_from_pid = /^$/]
    lib.firstSubPage = CONTENT
    lib.firstSubPage {
        table = pages
        select {
            #where = doktype=254 AND hidden=0
            where.insertData = 1
            pidInList.data = TSFE:id
            orderBy = sorting ASC
            max = 1
        }
        renderObj = TEXT
        renderObj {
            data = field:uid
            #value = Link to first child page
            #typolink {
            #  parameter.field = uid
            #}
        }
    }
[global]