##
## Page Template with Backend Layout
##
page {
    10 {
        templateRootPaths {
            300 = EXT:hive_cpt_nav_anchor/Resources/Private/Templates/Page
        }
    }
}

plugin.tx_hive_cpt_cnt_nav_anchor {
    settings {
        production {
            includePath {
                public = {$plugin.tx_hive_cpt_nav_anchor.settings.production.includePath.public}
                private = {$plugin.tx_hive_cpt_nav_anchor.settings.production.includePath.private}
                frontend {
                    public = {$plugin.tx_hive_cpt_nav_anchor.settings.production.includePath.frontend.public}
                }
            }
        }
    }
}

[globalVar = LIT:1 > {$plugin.tx_hive_cfg_typoscript.settings.gulp}]
page {
    includeCSS {
        tx_hive_cpt_cnt_nav_anchor_less = {$plugin.tx_hive_cpt_nav_anchor.settings.production.includePath.private}Assets/Less/includeCSS/index.less
    }
}
[global]