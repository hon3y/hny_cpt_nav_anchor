################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            SectionAnchor2020202020WithOffset {
                title = hive Backend Layout :: Section A/B/C with Offset :: 20% 20% 20% 20% 20%
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [0] SECTION A/B/C with Offset :: 20%
                                        colPos = 0
                                        colspan = 2
                                    }
                                    2 {
                                        name = [1] SECTION A/B/C with Offset :: 20%
                                        colPos = 1
                                        colspan = 2
                                    }
                                    3 {
                                        name = [2] SECTION A/B/C with Offset :: 20%
                                        colPos = 2
                                        colspan = 2
                                    }
                                    4 {
                                        name = [3] SECTION A/B/C with Offset :: 20%
                                        colPos = 3
                                        colspan = 2
                                    }
                                    5 {
                                        name = [4] SECTION A/B/C with Offset :: 20%
                                        colPos = 4
                                        colspan = 2
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2020202020WithOffset.gif
            }
        }
    }
}