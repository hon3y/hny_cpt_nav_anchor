################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            SectionAnchor5050 {
                title = hive Backend Layout :: Section A/B/C :: 50% 50%
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [0] SECTION A/B/C :: 50%
                                        colPos = 0
                                        colspan = 6
                                    }
                                    2 {
                                        name = [1] SECTION A/B/C :: 50%
                                        colPos = 1
                                        colspan = 6
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor5050.gif
            }
        }
    }
}