################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            SectionAnchor6040 {
                title = hive Backend Layout :: Section A/B/C :: 60% 40%
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [0] SECTION A/B/C :: 60%
                                        colPos = 0
                                        colspan = 7
                                    }
                                    2 {
                                        name = [1] SECTION A/B/C :: 40%
                                        colPos = 1
                                        colspan = 5
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor6040.gif
            }
        }
    }
}