################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            SectionAnchor7525 {
                title = hive Backend Layout :: Section A/B/C :: 75% 25%
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [0] SECTION A/B/C :: 75%
                                        colPos = 0
                                        colspan = 9
                                    }
                                    2 {
                                        name = [1] SECTION A/B/C :: 25%
                                        colPos = 1
                                        colspan = 3
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor7525.gif
            }
        }
    }
}